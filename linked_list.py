class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class Llist:
    def __init__(self):
        self.head = None
        self.tail = None

    def append(self, data):  # Adds a node at the end of the list
        x = Node(data)
        if self.tail is None:
            self.head = x
            self.tail = x
        else:
            self.tail.next = x
            self.tail = x

    def pop_first(self):  # pops first node of the list
        if self.head is None:
            raise Exception("Linked list is empty - cannot pop-out the first element")
        data = self.head.data
        node = self.head
        if self.head.next is None:
            self.head = None
            self.tail = None
        else:
            self.head = self.head.next
        del node
        return data
