# Maze solver

Solves a nxm maze that is taken from a file that has format of <br>
12110001<br>
10000100<br>
10110101<br>
11000121<br>

where 1 is a wall, 2 is an entrance or an exit and 0 is a walkable space

Returns a modified version of the file where 3 shows the path