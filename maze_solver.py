from input_parse import file_to_matrix
from linked_list import Llist
import copy


class MazeSolver:
    def __init__(self, input_file):
        self.maze = file_to_matrix(input_file)
        self.width = len(self.maze[0])
        self.height = len(self.maze)
        self.start_position = self.find_start()
        self.length = self.width*self.height
        self.positions = [None]*self.length
        self.maze_end = None
        self.solved_maze = None

    def find_start(self):   # looks for a 2 somewhere in the input file
        for i in range(self.height):
            for j in range(self.width):
                if self.maze[i][j] == "2":
                    return i*self.width + j

    def breadth_first_search(self):
        not_done = True
        queue = Llist()
        queue.append(self.start_position)
        adjacent_nodes_offset = [- self.width, -1, 1, self.width]
        while not_done:
            if queue.head is None:
                raise Exception("There is no path through the maze")
            current_node = queue.pop_first()
            for offset in adjacent_nodes_offset:  # visits adjacent nodes
                node_to_visit = offset + current_node
                if 0 < node_to_visit < self.length and self.positions[node_to_visit] is None:   # first conditions checks for staying in range of the maze, second one checks if the node was already visited
                    x = node_to_visit % self.width
                    y = ((node_to_visit - x) // self.width)
                    if self.maze[y][x] != "1" and abs(x - current_node % self.width) <= 1:  # first condition checks for wall, second blocks overflowing from left to right
                        self.positions[node_to_visit] = current_node
                        queue.append(node_to_visit)
                        if self.maze[y][x] == "2" and node_to_visit != self.start_position:  # checks for the end of the maze
                            self.maze_end = node_to_visit
                            not_done = False

    def trace_path(self):  # stores path into self.solved_maze
        self.solved_maze = copy.deepcopy(self.maze)
        position = self.maze_end
        while self.positions[position] != self.start_position:
            x = self.positions[position] % self.width
            y = (self.positions[position] - x) // self.width
            self.solved_maze[y][x] = "3"
            position = self.positions[position]

    def solve_maze(self):
        self.breadth_first_search()
        self.trace_path()
        file = open("solution", "w")
        for line in self.solved_maze:
            file.write("".join(line)+"\n")

    def create_ppm_file(self, draw_solved=False):  # handles drawing ppm files
        ppmfile = "P3 " + str(self.width) + " " + str(self.height) + " 255\n"
        color_table = {
            "0": "255 255 255",
            "1": "0 0 0",
            "2": "0 255 0",
            "3": "255 0 0",
        }
        if draw_solved:
            maze = self.solved_maze
        else:
            maze = self.maze

        for lines in maze:
            for columns in lines:
                ppmfile += color_table[columns] + " "
            ppmfile += "\n"

        if draw_solved:
            output_name = "solution.ppm"
        else:
            output_name = "input.ppm"
        file = open(output_name, "w")
        file.write(ppmfile)
