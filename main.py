import maze_solver

x = maze_solver.MazeSolver("input")

x.create_ppm_file()
x.solve_maze()
x.create_ppm_file(True)
